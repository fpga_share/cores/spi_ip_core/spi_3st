/*******************************************************************************
    Описание:
                Пакет с определениями типов данных IP ядра spi_3st
    Комментарии:
                
    License:
        This library is free software; you can redistribute it and/or           
        modify it under the terms of the GNU Lesser General Public              
        License as published by the Free Software Foundation; either            
        version 2.1 of the License, or (at your option) any later version.      
                                                                                
        This library is distributed in the hope that it will be useful,         
        but WITHOUT ANY WARRANTY; without even the implied warranty of          
        MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU       
        Lesser General Public License for more details.                         
                                                                                
        You should have received a copy of the GNU Lesser General Public        
        License along with this library; if not, write to the Free Software     
        Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111*1307 USA
                                                                                
                ***  GNU LESSER GENERAL PUBLIC LICENSE  ***                     
                  from http://www.gnu.org/licenses/lgpl.txt                     

    Разработчик:
        Bolshevik <bolshevikov.igor@gmail.com>
*******************************************************************************/

package spi_3st_pkg;

    import aux_functions::*;
    import custom_types_pkg::*;

    /**
     * количество символов в слове
     */
    localparam SYMBOLS_PER_WORD     = 4;
    /**
     * количество бит в символе
     */
    localparam BITS_PER_SYMBOL      = 8;
    /**
     * Размер регистра SPI
     */
    localparam SPI_REG_SIZE         = SYMBOLS_PER_WORD * BITS_PER_SYMBOL;
    /**
     * Количество сиимволов в транзакции чтения/записи
     */
    localparam SPI_TRANS_SIZE       = 4;
    /**
     * Максимальная длина одной транзакции в битах
     */
    localparam SPI_MAX_PACK_LEN     = 128;
    /**
     * Количество регистров описывающих данные одной транзакции
     */
    localparam SPI_DAT_REGS_COUNT   =   SPI_MAX_PACK_LEN
                                      / SPI_REG_SIZE;
    /**
     * Ширина шины адреса (в символьном предствалении)
     */
    localparam ADDR_SYMB_WIDTH      = 6;
    /**
     * Ширина шины адреса (в словесном предствалении)
     */
    localparam ADDR_WORD_WIDTH      = (ADDR_SYMB_WIDTH - $clog2(SYMBOLS_PER_WORD));
    /**
     * Ширина регистра делителя частоты
     */
    localparam DIVR_WIDTH           = 8;
    /**
     * Ширина регистра выбора слейва
     */
    localparam SSR_WIDTH            = 8;
    /**
     * Ширина регистра длины транзакции
     */
    localparam PACK_LEN_WIDTH       = $clog2(SPI_MAX_PACK_LEN);
    /**
     * Длительность задержки предустановки/удержания SS в тактах системной 
     * частоты
     */
    localparam SS_DELAY_LEN         = 2;
    /**
     * Размер счетчика предустановки/удержания SS
     */
    localparam SS_DELAY_WIDTH       = $clog2(SS_DELAY_LEN);

    /*
     * структура CR
     */
    typedef struct packed
    {
        /** поле выбора слейва */
        logic [lindex(SSR_WIDTH):0]         ss;
        /** флаг указывающий полярность сигнала ss. 1 - логика положительная */
        logic                               ss_is_positive;
        /** активация режима sdio */
        logic                               en_3st;
        /** активация режима автоматического управления линией SS */
        logic                               ass;
        /** поле разрешающее прерывания */
        logic                               ie;
        /**
         * поле указывающее что транзакции должны начинаться с младшего, 0-го
         * бита (data[0])
         */
        logic                               lsb_mode;
        /**
         * поле устанавливающее режим формирования посылки по положительному 
         * фронту тактирующего сигнала
         */
        logic                               cpha_tx;
        /**
         * поле устанавливающее режим захвата входных данных по положительному 
         * фронту тактирующего сигнала
         */
        logic                               cpha_rx;
        /**
         * поле устанавливающее начальное состояние линии тактового сигнала sclk
         */
        logic                               cpol;
        /**
         * флаг включающий кольцевой режим, т.е. соеддиняющий mosi и miso
         * Внимание!
         *     Использовать только с выключенным режимом 3st.
         */
        logic                               loopback_mode;
        /** флаг запускающий транзакцию */
        logic                               run;
        /* 
         * количество тактов в двунаправленной посылке (следующие первыми)
         * которые предназначены для передачи, оставшиеся такты будут 
         * использоаваться для приема
         */
        logic[lindex(PACK_LEN_WIDTH):0]     o_len;
        /** размер одой транзакции в битах */
        logic[lindex(PACK_LEN_WIDTH):0]     trans_len;
    } spi_cr_t;

    typedef struct packed
    {
        /* регистр делителя частоты */
        logic [lindex(DIVR_WIDTH):0]            divider_r;  /* MSB */
        /* контрльный регистр */
        spi_cr_t                                cr;
        /* массив принятых слов в одной транзакции */
        logic [lindex(SPI_DAT_REGS_COUNT):0]
              [lindex(SPI_REG_SIZE):0]          rr;
        /* массив слов для передачи в одной транзакции */
        logic [lindex(SPI_DAT_REGS_COUNT):0]
              [lindex(SPI_REG_SIZE):0]          tr;
    } spi_ctrl_t;

    /* размер структуры spi_ctrl_t в словах размером SPI_REG_SIZE */
    localparam SPI_CTRL_T_SIZE = (  $bits(spi_ctrl_t)
                                  / SPI_REG_SIZE        );

    /**
     * Массив смешений словесных адресов регистров данных чтения/записи
     */
    typedef enum logic[lindex(ADDR_WORD_WIDTH):0]
    {
        word_tx0,
        word_tx1,
        word_tx2,
        word_tx3,
        word_rx0,
        word_rx1,
        word_rx2,
        word_rx3,
        word_cr,
        word_div
    } offsets_t;

    localparam [lindex(ADDR_WORD_WIDTH):0] TX_OFFSTS = 
        {word_tx3, word_tx2, word_tx1, word_tx0};
    localparam [lindex(ADDR_WORD_WIDTH):0] RX_OFFSTS = 
        {word_rx3, word_rx2, word_rx1, word_rx0};

    /**
     * Тип перечисления определяющего состояния конечного автомата модуля 
     * формирования сигнала sclk
     */
    typedef enum logic[lindex(2):0]
    {
        DIS,
        PRESET_SS_TIMER,
        DATA_TRANSACT,
        HOLD_SS_TIMER
    } ss_ctrl_t;

    /**
     * Тип перечисления определяющего состояния свитча направления линии sdio
     */
    typedef enum logic
    {
        TRANSMITT,
        RECIEVE
    } dir_3st_switch_t;
    
endpackage : spi_3st_pkg

