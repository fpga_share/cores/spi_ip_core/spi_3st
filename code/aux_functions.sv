/*******************************************************************************
    Описание:
                Вспомогательные функции
    Комментарии:
                
    License:
        This library is free software; you can redistribute it and/or           
        modify it under the terms of the GNU Lesser General Public              
        License as published by the Free Software Foundation; either            
        version 2.1 of the License, or (at your option) any later version.      
                                                                                
        This library is distributed in the hope that it will be useful,         
        but WITHOUT ANY WARRANTY; without even the implied warranty of          
        MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU       
        Lesser General Public License for more details.                         
                                                                                
        You should have received a copy of the GNU Lesser General Public        
        License along with this library; if not, write to the Free Software     
        Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111*1307 USA
                                                                                
                ***  GNU LESSER GENERAL PUBLIC LICENSE  ***                     
                  from http://www.gnu.org/licenses/lgpl.txt                     
        
    Разработчик:
        Bolshevik <bolshevikov.igor@gmail.com>
*******************************************************************************/

package aux_functions;

    /***************************************************************************
     * Вычисление логарифма по основанию 2 от аргумента
     **************************************************************************/
    localparam ceil_log2_max_precision = 32;
    function integer ceil_log2 ;
        input [ceil_log2_max_precision-1:0] input_num;
        integer i;
        reg [ceil_log2_max_precision-1:0] try_result;
        begin
            i = 0;
            try_result = 1;
            while (  (try_result << i) < input_num
                     && i < ceil_log2_max_precision
                  )
                i = i + 1;
                
            ceil_log2 = i;
        end
    endfunction

    /***************************************************************************
     * Функция конвертирвоания адреса в байтовом представлении в адрес
     * в словесном представлении
     **************************************************************************/
    function integer byte2word ;
        input integer byte_addr;
        begin
            byte2word = byte_addr << 2;
        end
    endfunction

    /***************************************************************************
     * Функция конвертирвоания адреса в словесном 
     * представлении в адрес в байтовом представлении
     **************************************************************************/
    function integer word2byte ;
        input integer word_addr;
        begin
            word2byte = word_addr >> 2;
        end
    endfunction

    /***************************************************************************
     * Поиск максимального значения среди элементов массива
     *
     * на вход подается массив ака integer - data_in
     * и размер массива arr_width.
     * Т.к. реально нельзя (или я об этом не знаю) передать
     * параметром размер входного сигнала, то задаем максимальное 
     * его значение - MAX_ARR_WIDTH, а разрядность элементов
     * массива - ARR_VECT_WIDTH (вообще это подразумевает
     * тип integer, но Quartus может не принять порт типа integer)
     **************************************************************************/
    `define MAX_ARR_WIDTH   512
    `define ARR_VECT_WIDTH  32
    function  int findmax ;
        input integer arr_width;
        input [`MAX_ARR_WIDTH-1:0][`ARR_VECT_WIDTH-1:0] data_in;
        automatic integer tmp_max = 0;
        for (int i = 0; i < arr_width; i++) begin
            if(data_in[i] >= tmp_max) begin
                tmp_max = data_in[i];
            end
        end
        findmax = tmp_max;
    endfunction

    /***************************************************************************
     * Возвращает большее из двух чисел
     **************************************************************************/
    function automatic int max(
        bit [31:0] one,
        bit [31:0] two
    );
        if(one > two)
            return one;
        else
            return two;
    endfunction 

    /***************************************************************************
     * Поиск количества символов в слове при известной длине символа 
     * (число округляется до большего, если ширина шины не кратна длине символа)
     **************************************************************************/
    function int numb_of_symb;
        input int symbol_width;
        input int data_width;

        int i;
        begin
            i = 0;
            while (i*symbol_width < data_width)
                i = i + 1;
            numb_of_symb = i;
        end
    endfunction : numb_of_symb

    /***************************************************************************
     * Проверяет делятся ли два числа нацело.
     *     Если да - возвращает их делитель;
     *     если нет - возвращает делитель a/b + 1
     **************************************************************************/
    function int div_round;
        input int a;
        input int b;

        int tmp;
        begin
            tmp = a/b;
            if(tmp*b == a) begin
                div_round = tmp;
            end else 
                div_round = tmp + 1;
        end
    endfunction : div_round


    /***************************************************************************
     * Возвращает левй индекс декларируемого вектора. Если индекс изначально
     * равен 0 то возвращает 0, в противном случае width-1
     **************************************************************************/
    function automatic int lindex(
            bit [31:0] width
        );
        lindex = (width > 0) ? (width-1) : 0;
    endfunction : lindex

endpackage


