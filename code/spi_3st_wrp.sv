/*******************************************************************************
    Описание:
                Обертка преобразующая SV интерфейсы Avalon-MM в обычные wire's
                Может быть удобно при интеграции модуля в системы не имеющие
                интерфейсов
    Комментарии:
                
    License:
        This library is free software; you can redistribute it and/or           
        modify it under the terms of the GNU Lesser General Public              
        License as published by the Free Software Foundation; either            
        version 2.1 of the License, or (at your option) any later version.      
                                                                                
        This library is distributed in the hope that it will be useful,         
        but WITHOUT ANY WARRANTY; without even the implied warranty of          
        MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU       
        Lesser General Public License for more details.                         
                                                                                
        You should have received a copy of the GNU Lesser General Public        
        License along with this library; if not, write to the Free Software     
        Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111*1307 USA
                                                                                
                ***  GNU LESSER GENERAL PUBLIC LICENSE  ***                     
                  from http://www.gnu.org/licenses/lgpl.txt                     
        
    Разработчик:
        Bolshevik <bolshevikov.igor@gmail.com>
*******************************************************************************/

// synthesis translate_off
timeunit      1ps;
timeprecision 1ps;
// synthesis translate_on

import aux_functions::*;
import spi_3st_pkg::*;

module spi_3st_wrp
    (
        /* сигналы тактирования и сброса */
        input  wire                                 clk,
        input  wire                                 reset_n,

        /* интерфейс мастера */
        input  wire [lindex(ADDR_SYMB_WIDTH):0]     amm_s_address,
        input  wire [lindex(ceil_log2(
                           SYMBOLS_PER_WORD
                         * BITS_PER_SYMBOL)):0]     amm_s_byteenable,
        input  wire                                 amm_s_read,
        input  wire                                 amm_s_write,
        input  wire [lindex(
                           SYMBOLS_PER_WORD
                         * BITS_PER_SYMBOL):0]      amm_s_writedata,
        input  wire                                 amm_s_burstcount,
        output wire [lindex(
                           SYMBOLS_PER_WORD
                         * BITS_PER_SYMBOL):0]      amm_s_readdata,
        output wire                                 amm_s_waitrequest,
        output wire                                 amm_s_readdatavalid,
        input  wire                                 amm_s_debugaccess,

        /* выходы сигналов шины SPI */
        output wire [lindex(SSR_WIDTH):0]           ss_n,
        output wire                                 sclk,
        output wire                                 mosi,
        input  wire                                 miso,
        inout  wire                                 sdio,
        output wire                                 irq
    );

    /* декларация интерфейса писателя */
    amm_bus
        #(
            .ADDR_WIDTH             (ADDR_SYMB_WIDTH        ),
            .BITS_PER_SYMBOL        (BITS_PER_SYMBOL        ),
            .SYMBOLS_PER_WORD       (SYMBOLS_PER_WORD       ),
            .BURSTCNT_WIDTH         (1                      )
        )
        amm_if(
            .clk                    (clk                    )
        );

    spi_3st_top
        spi_3st_top_inst
        (
            /* сигналы тактирования и сброса */
            .clk                    (clk                    ),
            .reset_n                (reset_n                ),

            /* интерфейс amm шины управления */
            .amm_iface              (amm_if.amm_slave_mp    ),

            /* выходы сигналов шины SPI */
            .ss_n                   (ss_n                   ),
            .sclk                   (sclk                   ),
            .mosi                   (mosi                   ),
            .miso                   (miso                   ),
            .sdio                   (sdio                   ),
            .irq                    (irq                    )
        );

    assign amm_if.address       = amm_s_address;
    assign amm_if.byteenable    = amm_s_byteenable;
    assign amm_if.read          = amm_s_read;
    assign amm_if.write         = amm_s_write;
    assign amm_if.writedata     = amm_s_writedata;
    assign amm_if.burstcount    = amm_s_burstcount;

    assign amm_s_readdata       = amm_if.readdata;
    assign amm_s_waitrequest    = amm_if.waitrequest;
    assign amm_s_readdatavalid  = amm_if.readdatavalid;

endmodule // soc_mailbox_wrp