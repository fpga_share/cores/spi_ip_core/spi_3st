/*******************************************************************************
    Описание:
                Определения часто используемых разммеров сигнальных шин 
                Avalon-ST/Avalon-MM
    Комментарии:
                
    License:
        This library is free software; you can redistribute it and/or           
        modify it under the terms of the GNU Lesser General Public              
        License as published by the Free Software Foundation; either            
        version 2.1 of the License, or (at your option) any later version.      
                                                                                
        This library is distributed in the hope that it will be useful,         
        but WITHOUT ANY WARRANTY; without even the implied warranty of          
        MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU       
        Lesser General Public License for more details.                         
                                                                                
        You should have received a copy of the GNU Lesser General Public        
        License along with this library; if not, write to the Free Software     
        Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111*1307 USA
                                                                                
                ***  GNU LESSER GENERAL PUBLIC LICENSE  ***                     
                  from http://www.gnu.org/licenses/lgpl.txt                     
        
    Разработчик:
        Bolshevik <bolshevikov.igor@gmail.com>
*******************************************************************************/

package custom_types_pkg;

    import aux_functions::ceil_log2;

    /** системный размер символа */
    localparam SYMBOL_WIDTH             = 8;

    /** параметры описываюшие ширины сигналов 512-разрядной шины Avalon-ST */
    localparam DATA_512_WIDTH           = 512;
    localparam EMPTY_512_WIDTH          =   (DATA_512_WIDTH>SYMBOL_WIDTH)
                                          ? ceil_log2(DATA_512_WIDTH/SYMBOL_WIDTH)
                                          : 1;
    localparam BYTEEN_512_WIDTH         = (DATA_512_WIDTH/SYMBOL_WIDTH);

    /** параметры описываюшие ширины сигналов 256-разрядной шины Avalon-ST */
    localparam DATA_256_WIDTH           = 256;
    localparam EMPTY_256_WIDTH          =   (DATA_256_WIDTH>SYMBOL_WIDTH)
                                          ? ceil_log2(DATA_256_WIDTH/SYMBOL_WIDTH)
                                          : 1;
    localparam BYTEEN_256_WIDTH         = (DATA_256_WIDTH/SYMBOL_WIDTH);

    /** параметры описываюшие ширины сигналов 128-разрядной шины Avalon-ST */
    localparam DATA_128_WIDTH           = 128;
    localparam EMPTY_128_WIDTH          =   (DATA_128_WIDTH>SYMBOL_WIDTH)
                                          ? ceil_log2(DATA_128_WIDTH/SYMBOL_WIDTH)
                                          : 1;
    localparam BYTEEN_128_WIDTH         = (DATA_128_WIDTH/SYMBOL_WIDTH);

    /** параметры описываюшие ширины сигналов 64-разрядной шины Avalon-ST */
    localparam DATA_64_WIDTH            = 64;
    localparam EMPTY_64_WIDTH           =   (DATA_64_WIDTH>SYMBOL_WIDTH)
                                          ? ceil_log2(DATA_64_WIDTH/SYMBOL_WIDTH)
                                          : 1;
    localparam BYTEEN_64_WIDTH          = (DATA_64_WIDTH/SYMBOL_WIDTH);

    /** параметры описываюшие ширины сигналов 32-разрядной шины Avalon-ST */
    localparam DATA_32_WIDTH            = 32;
    localparam EMPTY_32_WIDTH           =   (DATA_32_WIDTH>SYMBOL_WIDTH)
                                          ? ceil_log2(DATA_32_WIDTH/SYMBOL_WIDTH)
                                          : 1;
    localparam BYTEEN_32_WIDTH          = (DATA_32_WIDTH/SYMBOL_WIDTH);

    /** параметры описываюшие ширины сигналов 16-разрядной шины Avalon-ST */
    localparam DATA_16_WIDTH            = 16;
    localparam EMPTY_16_WIDTH           =   (DATA_16_WIDTH>SYMBOL_WIDTH)
                                          ? ceil_log2(DATA_16_WIDTH/SYMBOL_WIDTH)
                                          : 1;
    localparam BYTEEN_16_WIDTH          = (DATA_16_WIDTH/SYMBOL_WIDTH);

    /** параметры описываюшие ширины сигналов 8-разрядной шины Avalon-ST */
    localparam DATA_8_WIDTH             = 8;
    localparam EMPTY_8_WIDTH            =   (DATA_8_WIDTH>SYMBOL_WIDTH)
                                          ? ceil_log2(DATA_8_WIDTH/SYMBOL_WIDTH)
                                          : 1;
    localparam BYTEEN_8_WIDTH           = (DATA_8_WIDTH/SYMBOL_WIDTH);

endpackage /* custom_types_pkg */


