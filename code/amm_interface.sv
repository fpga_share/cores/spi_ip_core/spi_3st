/*******************************************************************************
    Описание:
                SV интерфейс шины Avalon-MM
    Комментарии:
                
    License:
        This library is free software; you can redistribute it and/or           
        modify it under the terms of the GNU Lesser General Public              
        License as published by the Free Software Foundation; either            
        version 2.1 of the License, or (at your option) any later version.      
                                                                                
        This library is distributed in the hope that it will be useful,         
        but WITHOUT ANY WARRANTY; without even the implied warranty of          
        MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU       
        Lesser General Public License for more details.                         
                                                                                
        You should have received a copy of the GNU Lesser General Public        
        License along with this library; if not, write to the Free Software     
        Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111*1307 USA
                                                                                
                ***  GNU LESSER GENERAL PUBLIC LICENSE  ***                     
                  from http://www.gnu.org/licenses/lgpl.txt                     

    Разработчик:
        Bolshevik <bolshevikov.igor@gmail.com>
*******************************************************************************/

// synthesis translate_off
timeunit      1ps;
timeprecision 1ps;
// synthesis translate_on

import aux_functions::ceil_log2;
import aux_functions::lindex;

interface amm_bus
    #(
        BITS_PER_SYMBOL     = 8,
        SYMBOLS_PER_WORD    = 4,
        ADDR_WIDTH          = 3,
        BURSTCNT_WIDTH      = 10
    )
    (
        input logic         clk
    );

    logic [lindex(ADDR_WIDTH):0]                address;
    logic [lindex(SYMBOLS_PER_WORD):0]          byteenable;
    logic                                       chipselect;
    logic                                       read;
    logic                                       write;
    logic [lindex(   BITS_PER_SYMBOL
                   * SYMBOLS_PER_WORD):0]       readdata;
    logic [lindex(   BITS_PER_SYMBOL
                   * SYMBOLS_PER_WORD):0]       writedata;
    logic                                       waitrequest;
    logic                                       readdatavalid;
    logic [lindex(BURSTCNT_WIDTH):0]            burstcount;
    logic                                       beginbursttransfer;
    
    modport amm_master_mp(
        output      address,
        output      byteenable,
        output      chipselect,
        output      read,
        output      write,
        output      writedata,
        output      burstcount,
        output      beginbursttransfer,
        input       readdata,
        input       waitrequest,
        input       readdatavalid
        );
    
    modport amm_slave_mp(
        input       address,
        input       byteenable,
        input       chipselect,
        input       read,
        input       write,
        input       writedata,
        input       burstcount,
        input       beginbursttransfer,
        output      readdata,
        output      waitrequest,
        output      readdatavalid
        );
    
endinterface : amm_bus

