/*******************************************************************************
    Описание:
                Модуль SPI трансивера с возможносьтю работы по линии с 3 
                состояниями
    Комментарии:
                

    License:
        This library is free software; you can redistribute it and/or           
        modify it under the terms of the GNU Lesser General Public              
        License as published by the Free Software Foundation; either            
        version 2.1 of the License, or (at your option) any later version.      
                                                                                
        This library is distributed in the hope that it will be useful,         
        but WITHOUT ANY WARRANTY; without even the implied warranty of          
        MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU       
        Lesser General Public License for more details.                         
                                                                                
        You should have received a copy of the GNU Lesser General Public        
        License along with this library; if not, write to the Free Software     
        Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111*1307 USA
                                                                                
                ***  GNU LESSER GENERAL PUBLIC LICENSE  ***                     
                  from http://www.gnu.org/licenses/lgpl.txt                     
        
    Разработчик:
        Bolshevik <bolshevikov.igor@gmail.com>
*******************************************************************************/

// synthesis translate_off
timeunit      1ns;
timeprecision 1ns;
// synthesis translate_on

import aux_functions::*;
import custom_types_pkg::*;
import spi_3st_pkg::*;

module spi_3st_transciever
    (
        /***********************************************************************
         * сигналы тактирования и сброса
         **********************************************************************/
        input  wire                                 clk,
        input  wire                                 reset_n,

        /***********************************************************************
         * сигналы даных
         **********************************************************************/
        /* слово передаваемых данных */
        input  wire [lindex(SPI_MAX_PACK_LEN):0]    data_in,
        /* слово принятных данных */
        output logic [lindex(SPI_MAX_PACK_LEN):0]   data_out,
        /***********************************************************************
         * сигналы управления
         **********************************************************************/
        /* 
         * слово длины сообщения в транзакции (для транзакции с двунаправленной
         * передачей это длина приемного + передаваемого сообщений)
         */
        input  wire [lindex(PACK_LEN_WIDTH):0]      trans_len,
        /* слово размера передаваемого сообщения (для режима SDIO) */
        input  wire [lindex(PACK_LEN_WIDTH):0]      tx_len,
        /* флаг выбора начального сотояния линии клока */
        input  wire                                 cpol_hi,
        /* 
         * флаг выбора фронта sclk для передаваемых данных 
         * (0 - fall, 1 - rise)
         */
        input  wire                                 cpha_tx,
        /* флаг выбора фронта sclk для принимаемых данных */
        input  wire                                 cpha_rx,
        /* флаг выбирающий с какой стороны передавать слово (MSB/LSB) */
        input  wire                                 msb,
        /* флаг активации режима петли (mosi->miso) */
        input  wire                                 loopback_mode,
        /* флаг команды начала транзакции */
        input  wire                                 run,
        /* флаг указывающий использовать режим 3st */
        input  wire                                 use_3st,

        /***********************************************************************
         * Сигналы от модуля формировования тактовой
         **********************************************************************/
        /* строб обозначающий событие появления переднего фронта sclk */
        input  logic                                rise_strob,
        /* строб обозначающий событие появления заднего фронта sclk */
        input  logic                                fall_strob,
        /* слово счетчика тактов от модуля формирования */
        input  logic [lindex(PACK_LEN_WIDTH):0]     tick_index,
        /* маркер начала транзакции на шине, появляется на один такт в начале */
        input  wire                                 start_event,

        /***********************************************************************
         * сигналы SPI
         **********************************************************************/
        output logic                                mosi,
        input  logic                                miso,
        inout  wire                                 sdio
    );

    /***************************************************************************
     * Типы данных, сигналы, константы
     **************************************************************************/
    /* 
     * если в регистр tx_len записано !0 значение - в текущей транзакции 
     * появятся данные для чтения
     */
    wire rw_3st_msg = |tx_len;
    /* 
     * строб по которому будут выставляться на шину биты для передачи
     * Т.к. данные на шину необходимо выставить заранее - порядок стробов 
     * инвертирован
     */
    wire wr_active_strb =   cpha_tx
                          ? fall_strob
                          : rise_strob;

    /* внутреннее представление miso не зависящее от режима входов */
    logic miso_local;
    /* внутреннее представление mosi не зависящее от режима выходов*/
    logic mosi_local;
    /* сигнал строба для записи */
    wire wr_load_strob;
    wire [lindex(PACK_LEN_WIDTH):0] wr_index;
    
    /*
     * Если режим spi == 1 или 2 (cpol != cpha) то возникает проблема с 
     * предустановкой бита на шину. Т.к. предустановка происходит по
     * противоположному фронту от рабочего то для первого бита необходимо
     * установить состояние mosi заранее (т.к. логика расчитывает что уже был 
     * проитвоположный фронт за который состояние установлено). Для этого 
     * используем вспомогательный бит start_event появляющийся перед началом
     * всей транзакции.
     * Кроме того возникает необходимость в изменении tick_index.
     */
    assign wr_load_strob =   (cpol_hi ^ cpha_tx)
                           ? wr_active_strb | start_event
                           : wr_active_strb;

    assign wr_index      =   (cpol_hi ^ cpha_tx)
                           ? tick_index - 1
                           : tick_index;

    /***************************************************************************
     * Кольцевой сдвиговый регистр формирования mosi при msb = 0
     **************************************************************************/
    logic [lindex(SPI_MAX_PACK_LEN):0]  wr_word;
    always_ff @(posedge clk or negedge reset_n) begin : proc_load_write_word
        if(~reset_n) begin
            wr_word <= '0;
        end else begin
            if(run) begin
                wr_word     <= data_in;
            end else if(wr_load_strob) begin
                wr_word <= {1'b0, wr_word[lindex(SPI_MAX_PACK_LEN):1]};
            end
        end
    end

    /***************************************************************************
     * Цикл формирования сигнала mosi
     **************************************************************************/
    always_ff @(posedge clk or negedge reset_n) begin : proc_mosi
        if(~reset_n) begin
            mosi_local <= '0;
        end else begin
            if(wr_load_strob) begin
                if(msb) begin
                    mosi_local  <= data_in[wr_index];
                end else begin
                    mosi_local  <= wr_word[0];
                end
            end
        end
    end

    /***************************************************************************
     * Цикл чтения состояния линии miso
     **************************************************************************/
    wire rd_active_strb =   cpha_rx
                          ? fall_strob
                          : rise_strob;
    logic [lindex(PACK_LEN_WIDTH):0]    rx_miso_cnt;
    always_ff @(posedge clk or negedge reset_n) begin : proc_get_miso
        if(~reset_n) begin
            data_out        <= '0;
            rx_miso_cnt     <= '0;
        end else begin
            if(run) begin
                if(msb) begin
                    rx_miso_cnt     <= trans_len;
                end else begin
                    rx_miso_cnt     <= '1;
                end
            end else if(rd_active_strb) begin
                if(msb) begin
                    rx_miso_cnt--;
                end else begin
                    rx_miso_cnt++;
                end
                data_out[rx_miso_cnt]    <= miso_local;
            end
        end
    end

    /***************************************************************************
     * Переключение режимов mosi/miso/sdio
     * 
     * Если режим 3st НЕ используется, то счетчик tx_3st_cnt всегда равен 0, а
     * сигнал dir_3st_switch всегда в состоянии TRANSMITT. При этом miso_local
     * соединен с входящим miso. Сиганл mosi соединен с mosi_local.
     *
     * Если используется режим 3st, то:
     *     - если длина tx_len равна 0 - dir_3st_switch всегда в состоянии 
     *       TRANSMITT, а sdio работает на передачу;
     *     - если tx_len != нулю, то первые tx_len бит сигнал dir_3st_switch 
     *       в состоянии TRANSMITT, а sdio передает. Последующие такты sclk
     *       сигнал dir_3st_switch в состоянии RECIEVE, а sdio работает на прием
     **************************************************************************/
    logic [lindex(PACK_LEN_WIDTH):0]    tx_3st_cnt;
    always_ff @(posedge clk or negedge reset_n) begin : proc_3st_cnt
        if(~reset_n) begin
            tx_3st_cnt <= '0;
        end else begin
            if(run) begin
                tx_3st_cnt  <= '0;
            end else if(use_3st & wr_active_strb) begin
                tx_3st_cnt++;
            end
        end
    end

    dir_3st_switch_t dir_3st_switch;
    always_ff @(posedge clk or negedge reset_n) begin : proc_3st_switch
        if(~reset_n) begin
            dir_3st_switch <= TRANSMITT;
        end else begin
            if(run) begin
                dir_3st_switch <= TRANSMITT;
            end else if(tx_3st_cnt == tx_len & rw_3st_msg) begin
                dir_3st_switch <= RECIEVE;
            end
        end
    end

    assign sdio =   dir_3st_switch == TRANSMITT
                  ? mosi_local
                  : 'z;

    assign mosi = mosi_local;

    assign miso_local =   loopback_mode
                        ? mosi_local
                        :   dir_3st_switch == RECIEVE
                          ? sdio
                          : miso & (!use_3st);
endmodule // spi_3st_transciever