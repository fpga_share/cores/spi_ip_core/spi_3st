/*******************************************************************************
    Описание:
                Топ-модуль проекта spi_3st
    Комментарии:
        Режимы SPI:
            CPOL=0 : клок в нективном состоянии находится в логическом 0;
            CPOL=1 : клок в нективном состоянии находится в логической 1;
            CPHA=0 : данный тактируются по ниспадающему фронту;
            CPHA=1 : данный тактируются по положительному фронту;
    License:
        This library is free software; you can redistribute it and/or           
        modify it under the terms of the GNU Lesser General Public              
        License as published by the Free Software Foundation; either            
        version 2.1 of the License, or (at your option) any later version.      
                                                                                
        This library is distributed in the hope that it will be useful,         
        but WITHOUT ANY WARRANTY; without even the implied warranty of          
        MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU       
        Lesser General Public License for more details.                         
                                                                                
        You should have received a copy of the GNU Lesser General Public        
        License along with this library; if not, write to the Free Software     
        Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111*1307 USA
                                                                                
                ***  GNU LESSER GENERAL PUBLIC LICENSE  ***                     
                  from http://www.gnu.org/licenses/lgpl.txt                     
        
    Разработчик:
        Bolshevik <bolshevikov.igor@gmail.com>
*******************************************************************************/

// synthesis translate_off
timeunit      1ps;
timeprecision 1ps;
// synthesis translate_on

import aux_functions::*;
import custom_types_pkg::*;
import spi_3st_pkg::*;

module spi_3st_top
    (
        /* сигналы тактирования и сброса */
        input  wire                         clk,
        input  wire                         reset_n,

        /* интерфейс amm шины управления */
        amm_bus                             amm_iface,

        /* выходы сигналов шины SPI */
        output wire [lindex(SSR_WIDTH):0]   ss_n,
        output wire                         sclk,
        output wire                         mosi,
        input  wire                         miso,
        inout  wire                         sdio,
        output wire                         irq
    );


    assign amm_iface.waitrequest    = '0;
    /***************************************************************************
     * Типы, сигналы, интерфейсы
     **************************************************************************/
    /* словесное представление адреса */
    wire [lindex(ADDR_WORD_WIDTH):0] word_addr = amm_iface.address >> 2;
    /* сигнал SS еще не назначенный конкретному потребителю */
    wire ss_local;

    // synthesis translate_off
    
    /* @note 
     *  assert может не поддерживаться freeware версиями ModelSim.
     */
    initial
        begin
            /* 
             * Проверим что размер типа равен размеру слова
             */
            check_csr_t: assert(
                ($bits(spi_3st_pkg::spi_cr_t) == SPI_REG_SIZE)
                )   
            else
                $error("spi_cr_t is not equal SPI_REG_SIZE!");
        end
    // synthesis translate_on

    /***************************************************************************
     * Запись регистров CR, TR через шину AMM
     **************************************************************************/
    /*
     * Запись в регистр CR
     */
    spi_ctrl_t ctrl_regs;
    always_ff @(posedge clk or negedge reset_n) begin : proc_cr_write
        if(~reset_n) begin
            ctrl_regs.cr <= '0;
        end else begin
            if(    word_addr == word_cr
                && amm_iface.write              ) begin
                ctrl_regs.cr <= amm_iface.writedata;
            end else begin
                ctrl_regs.cr.run <= '0;
            end
        end
    end
    
    /*
     * Запись в регистр передаваемых данных
     */
    genvar i;
    generate
        for (i = 0; i < SPI_TRANS_SIZE; i++) begin : test
            
            always_ff @(posedge clk or negedge reset_n) begin : proc_tr_write
                if(~reset_n) begin
                    ctrl_regs.tr[i] <= '0;
                end else begin
                    if(    word_addr == TX_OFFSTS[i]
                        && amm_iface.write              ) begin
                        ctrl_regs.tr[i] <= amm_iface.writedata;
                    end else begin
                        ctrl_regs.tr[i] <= ctrl_regs.tr[i];
                    end
                end
            end
        end
    endgenerate

    /*
     * Запись в регистр делителя частоты
     */
    always_ff @(posedge clk or negedge reset_n) begin : proc_write_div_reg
        if(~reset_n) begin
            ctrl_regs.divider_r <= '0;
        end else begin
            if(    word_addr == word_div
                && amm_iface.write      ) begin
                ctrl_regs.divider_r <= amm_iface.writedata;
            end else begin
                ctrl_regs.divider_r <= ctrl_regs.divider_r;
            end
        end
    end

    /***************************************************************************
     * Чтение регистров CR, RR через шину AMM
     **************************************************************************/
    wire [lindex(SPI_CTRL_T_SIZE):0]
         [lindex(SPI_REG_SIZE):0]       spi_ctrl_mem = ctrl_regs;
    always_ff @(posedge clk or negedge reset_n) begin : proc_cr_read
        if(~reset_n) begin
            amm_iface.readdata      <= '0;
            amm_iface.readdatavalid <= '0;
        end else begin
            amm_iface.readdata      <= spi_ctrl_mem[word_addr];
            amm_iface.readdatavalid <= amm_iface.read;
        end
    end

    /***************************************************************************
     * Удостоверимся что run будет активен только один такт
     **************************************************************************/
    logic run_r;
    logic run_rise;
    always_ff @(posedge clk or negedge reset_n) begin : proc_run_single
        if(~reset_n) begin
            run_rise    <= '0;
            run_r       <= '0;
        end else begin
            run_r       <= ctrl_regs.cr.run;
            run_rise    <= (!run_r & ctrl_regs.cr.run);
        end
    end

    /***************************************************************************
     * Назначение SS потребителям
     **************************************************************************/
    logic [lindex(SSR_WIDTH):0]  ss_bus_n;
    always_ff @(posedge clk or negedge reset_n) begin : proc_ss_select
        if(~reset_n) begin
            ss_bus_n <= '0;
        end else begin
            ss_bus_n <= {SSR_WIDTH{ss_local}} | ~ctrl_regs.cr.ss;
        end
    end
    /* 
     * по умолчанию сигнал ss имеет активный низкий уровень.
     * Если нужно - инвертируем
     */
    assign ss_n =   ctrl_regs.cr.ss_is_positive
                  ? ~ss_bus_n
                  : ss_bus_n;

    wire                                rise_strob;
    wire                                fall_strob;
    wire [lindex(PACK_LEN_WIDTH):0]     tick_index;
    wire                                start_event;

    spi_3st_transciever
        spi_3st_transciever_inst
        (
            .clk                    (clk                        ),
            .reset_n                (reset_n                    ),

            .data_in                (ctrl_regs.tr               ),
            .data_out               (ctrl_regs.rr               ),

            .trans_len              (ctrl_regs.cr.trans_len     ),
            .tx_len                 (ctrl_regs.cr.o_len         ),

            .cpol_hi                (ctrl_regs.cr.cpol          ),
            .cpha_tx                (ctrl_regs.cr.cpha_tx       ),
            .cpha_rx                (ctrl_regs.cr.cpha_rx       ),
            .msb                    (!ctrl_regs.cr.lsb_mode     ),
            .loopback_mode          (ctrl_regs.cr.loopback_mode ),
            .run                    (run_rise                   ),
            .use_3st                (ctrl_regs.cr.en_3st        ),

            .rise_strob             (rise_strob                 ),
            .fall_strob             (fall_strob                 ),
            .tick_index             (tick_index                 ),
            .start_event            (start_event                ),

            .mosi                   (mosi                       ),
            .miso                   (miso                       ),
            .sdio                   (sdio                       )
        );

    spi_3st_sclk_former
        spi_3st_sclk_former_inst
        (
            .clk                    (clk                        ),
            .reset_n                (reset_n                    ),

            .divider                (ctrl_regs.divider_r        ),
            .trans_len              (ctrl_regs.cr.trans_len     ),
            .cpol_hi                (ctrl_regs.cr.cpol          ),
            .run                    (run_rise                   ),
            .ie                     (ctrl_regs.cr.ie            ),

            .tick_index             (tick_index                 ),
            .rise_strob             (rise_strob                 ),
            .fall_strob             (fall_strob                 ),
            .sclk                   (sclk                       ),
            .ss_n                   (ss_local                   ),
            .irq                    (irq                        ),
            .start_event            (start_event                )
        );

endmodule // spi_3st_top